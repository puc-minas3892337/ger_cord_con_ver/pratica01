# Informações Pessoais
<p>
Nome: João Luiz Santana da Hora <br>
Matricula: 189345 <br>
E-mail: Joaao_luiz@hotmail.com
</p>

# Experiência
<p>
Tech Lead DevOps com uma formação em Análise e Desenvolvimento de Sistemas (UNICID) e uma pós-graduação em Arquitetura de Soluções pela FIAP, minha carreira tem sido marcada por uma trajetória diversificada e enriquecedora na área de tecnologia. Minha experiência abrange diversas facetas, desde a gestão de infraestrutura (DevOps) até o desenvolvimento de automações e software, bem como o gerenciamento de projetos.
</p>